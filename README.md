# DieBahn

A GTK4 frontend for the travel information of the german railway ("Reiseauskunft der Deutschen Bahn").

## Screenshots

![Overview of the application](https://gitlab.com/Schmiddiii/diebahn/-/raw/master/packaging/screenshots/overview.png)
![Smaller Screen](https://gitlab.com/Schmiddiii/diebahn/-/raw/master/packaging/screenshots/mobile.png)

## Features

- Search for a journey.
- List all available journeys.
- View the details of a journey including departure, arrivals, delays, platforms.
- Convergent for small screens.
- Bookmark a search or a journey.
- Show more information like prices.

## Contact

There is a matrix room setup for this application. You can join it [here](https://matrix.to/#/%23diebahn:matrix.org?via=matrix.org) if you want.
